from connect_2_db import Connect
from feature_table import createFeatures
import datetime
import sys

granularity = 5.0          # in minutes
history = 6 * 30           # in feature days
tlag = 4                   # in time steps
time_aheads = [5, 15, 60]  # in minutes  #CHANGED ----------------- IMP

# Set drop = True only when dropping feature table
drop = True                                   
# drop = False
# =====================================
# CREATE VECTOR SPACE IN DATABASE
# =====================================

def vector_space_creation():
    
    con = Connect()
    cur = con.cur
    global drop
    
    query = "SELECT DISTINCT `SystemCodeNumber` FROM utmc_transport_link_data_dynamic"
    cur.execute(query)
    SCN = cur.fetchall()
    
    
    # To create features for all the sensors 
    for i in range(len(SCN)):
        
        scn = SCN[i][0]
        # scn = "LINK002"
        print("Feautes of " + scn + " is being loaded...")
        query = "SELECT `LastUpdated` FROM `utmc_transport_link_data_dynamic` WHERE `Reality` = 'Detector' ORDER BY `LastUpdated` DESC LIMIT 1"
        cur.execute(query)
        lasttime = cur.fetchone()[0] 
        
        #Passing scn and last updated time stamp         
        drop = createFeatures(lasttime, tlag, scn, granularity, history, time_aheads, cur, drop)
        # sys.exit(0)
    
    # Uncomment below and comment above code to create features for each sensors separately
    '''
    scn = 'J007_L03_02'
    lasttime = datetime.datetime.strptime("2019-11-23 00:30:00","%Y-%m-%d %H:%M:%S")
    createFeatures(lasttime, tlag, scn, granularity, history, time_aheads, cur, drop)
    '''
    con.close()

# Takes too much time - Create features for one detector at a time
if __name__ == '__main__':
    
    vector_space_creation()
    
    '''
    WARNING: This code can delete or drop large number of features and the feature creation may take long time 
             to create features again from the beginning
    '''

