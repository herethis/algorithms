from connect_2_db import Connect
from OLS_feedback import update_OLS_table

# =====================================
# UPDATE OLS TABLE
# =====================================

tlag        = 4
granularity = 5
time_aheads = [5, 15, 60]
ols_hist    = 2

drop = True

def create_OLS_params():
    
    con = Connect()
    cur = con.cur
    global drop
    
    query = "SELECT DISTINCT `SystemCodeNumber` FROM `utmc_transport_link_data_dynamic` WHERE `Reality` = 'Detector'"
    cur.execute(query)
    SCN = cur.fetchall()
    
    for i in range(len(SCN)):
        scn = SCN[i][0]
        
        print("--------------------------------------------------")
        print("Updating OLS parameters for %s"%(scn))
        
        drop = update_OLS_table(scn, drop, tlag, granularity, time_aheads, ols_hist, cur)

        print("Updation finished for sensor : %s"%(scn))
        print("--------------------------------------------------")        
        
        drop = False
        print('Set drop = False')
    
    con.close()

if __name__ == '__main__':

    # create_OLS_params - creates parameters for error predicton on the basis of last two days error values    
    create_OLS_params()

