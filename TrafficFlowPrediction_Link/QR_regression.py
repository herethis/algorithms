from sklearn.preprocessing import PolynomialFeatures
#from sklearn.linear_model import LinearRegression
from sklearn import linear_model

def QR_train(train, new_v):
    #print len(train)
    #print len(train[0])
    X_train = train[:,:-1]
    y_train = train[:,-1]

    #print X_train
    #print y_train
    # # create a Linear Regressor   
    #lin_regressor = LinearRegression()

    reg = linear_model.Ridge(alpha=5.0)
    # # pass the order of your polynomial here  
    poly = PolynomialFeatures(1)

    # # convert to be used further to linear regression
    X_transform = poly.fit_transform(X_train)

    # print X_transform

    # # fit this to Linear Regressor
    #lin_regressor.fit(X_transform,y_train)
    reg.fit(X_transform, y_train) 

    X_test_t = poly.fit_transform([new_v])
    #print reg.coef_
    # # get the predictions
    #y_pred = lin_regressor.predict(X_test_t)
    y_pred = reg.predict(X_test_t)
    return y_pred
