# This code updates Flow_status_type_id in utmc_detector_dynamic table
# This code needs slight modifications.
from con import Con
import numpy as np
import datetime
import sys
from linkCode import SysCodeNum
import calendar
import time

granularity = 5
# True -> belongs to outlier

def isOutlier(data, cut):
	med = np.median(data)
	mad = 1.4826 * np.median(abs(data - med))
	if mad < 1e-9:
		return False
	eps = abs(data - med) / mad
	#print mad, eps[-1]
	#print eps
	if eps[-1] > cut:
		return True
	return False

def flow_merge(flow1, flow2):
	memo = {}
	flows = []
	for f in flow1:
		if f[1] == None:
			continue
		t = f[0]
		t = t - datetime.timedelta(seconds = t.second)
		rem = (t.minute)%5
		t = t - datetime.timedelta(minutes = rem)
		memo[t] = float(f[1])

	for f in flow2:
		if f[1] == None:
			continue
		t = f[0]
		t = t - datetime.timedelta(seconds = t.second)
		rem = (t.minute)%5
		t = t - datetime.timedelta(minutes = rem)
		if t in memo:
			flows.append(memo[t] + float(f[1]))

	return flows


def lastdays(lasttime, duration, links):
	
	con = Con()
	cur = con.cur
	flows = []
	up = lasttime
	down = lasttime + datetime.timedelta(minutes = -60)
	query = "SELECT `LastUpdated`, `TotalFlow` FROM `utmc_detector_dynamic` WHERE `LastUpdated` >= '%s' AND `LastUpdated` < '%s' AND `SystemCodeNumber` = '%s'" %(str(down), str(up), links[0])
	cur.execute(query)
	flow1 = cur.fetchall()
	
	query = "SELECT `LastUpdated`, `TotalFlow` FROM `utmc_detector_dynamic` WHERE `LastUpdated` >= '%s' AND `LastUpdated` < '%s' AND `SystemCodeNumber` = '%s'" %(str(down), str(up), links[1])
	cur.execute(query)
	flow2 = cur.fetchall()
	flows.extend(flow_merge(flow1, flow2))

	up = lasttime + datetime.timedelta(days = -1)
	up = up + datetime.timedelta(minutes = 30)
	down = lasttime + datetime.timedelta(days = -7)
	down = down + datetime.timedelta(minutes = -30)
	query = "SELECT `LastUpdated`, `TotalFlow` FROM `utmc_detector_dynamic` WHERE DATE(`LastUpdated`) >= '%s' AND DATE(`LastUpdated`) < '%s' AND TIME(`LastUpdated`) >= '%s' AND TIME(`LastUpdated`) < '%s' AND`SystemCodeNumber` = '%s'" %(str(down.date()), str(up.date()), str(down.time()), str(up.time()), links[0])
	cur.execute(query)
	flow1 = cur.fetchall()
	
	query = "SELECT `LastUpdated`, `TotalFlow` FROM `utmc_detector_dynamic` WHERE DATE(`LastUpdated`) >= '%s' AND DATE(`LastUpdated`) < '%s' AND TIME(`LastUpdated`) >= '%s' AND TIME(`LastUpdated`) < '%s' AND`SystemCodeNumber` = '%s'" %(str(down.date()), str(up.date()), str(down.time()), str(up.time()), links[1])
	cur.execute(query)
	flow2 = cur.fetchall()
	flows.extend(flow_merge(flow1, flow2))

	#print flows
	return flows

def outlier_classifier(lasttime, links, flow):
	#link = links[: 8]
	# 7 days flow
	duration = 7
	data = lastdays(lasttime, duration, links)
	data.append(flow)
	data = np.array(data)
	cut = 2.0
	if lasttime.hour > 6 and lasttime.hour < 22:
		cut = 3.5
	return isOutlier(data, cut)


while True:

	start_class = datetime.datetime.now()
	con = Con()
	cur = con.cur
	query = "SELECT `SystemCodeNumber`, `TransportLinkRef` FROM `utmc_transport_link_static` WHERE 1"
	cur.execute(query)
	SCN = cur.fetchall()
	for i in range(len(SCN)):
		try:
			scn = SCN[i][0]
			link = SysCodeNum(SCN[i][1], cur)
			# print link, scn
			if link == None:
				print "No detectors found for %s" %(scn)
				continue
			timenow = datetime.datetime.now()
			query = "SELECT `SystemCodeNumber`, `LastUpdated`, `TotalFlow` FROM `utmc_detector_dynamic` WHERE (`SystemCodeNumber` = '%s' OR `SystemCodeNumber` = '%s') ORDER BY `LastUpdated` DESC LIMIT 2" %(link[0], link[1])
			# print query
			cur.execute(query)
			result = cur.fetchall()
			if len(result) == 0:
				continue
			# print result
			lastflow_a = None
			lasttime = result[0][1]
			if len(result) == 2 and result[0][0] != result[1][0]:
				t_1 = result[0][1] - datetime.timedelta(seconds = result[0][1].second)
				rem = (t_1.minute)%5
				t_1 = t_1 - datetime.timedelta(minutes = rem)
				t_2 = result[1][1] - datetime.timedelta(seconds = result[1][1].second)
				rem = (t_2.minute)%5
				t_2 = t_2 - datetime.timedelta(minutes = rem)
				if t_1 == t_2 and result[0][2] != None and result[1][2] != None:
					lastflow_a = result[0][2] + result[1][2]
			
			lasttime = lasttime - datetime.timedelta(seconds = lasttime.second)
			rem = (lasttime.minute)%5
			lasttime = lasttime - datetime.timedelta(minutes = rem)
			print lastflow_a
			if lastflow_a == None:
				continue
			lastflow_a = float(lastflow_a)

			# Last updated flow was an outllier or not
			if outlier_classifier(lasttime, link, lastflow_a):
				#query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; "
				query = "UPDATE `utmc_detector_dynamic` SET `FlowStatus_TypeID` = '%d' WHERE `LastUpdated` >= '%s' AND (`SystemCodeNumber` = '%s' OR `SystemCodeNumber` = '%s');" %(1, str(lasttime), link[0], link[1])
				#query = query + " SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;"
				print query
				cur.execute(query)

		except Exception as error:
			print "======================="
			print error
			print "======================="

	print "Outlier Classifier took: ........ ", datetime.datetime.now() - start_class
	time_taken = (datetime.datetime.now() - start_class).seconds
	sleep_time = granularity*60 - time_taken
	if sleep_time > 0:
		time.sleep(sleep_time)

	con.close()
