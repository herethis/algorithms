from connect_2_db import Connect
import numpy as np
import datetime
import calendar
import sys

from QR_regression import QR_train
from profile_table import historic_profile
from OLS_feedback  import OLS_Vector
from feature_table import update_feature_table, Single_Vector, find_neighbours
from outliers_flow import outlier_classifier

granularity = 5.0           # in minutes
tlag = 4                    # in time steps
tnn = 40                    # no of vectors
ols_hist = 2                # in days
time_aheads = [5, 15, 60]   # in minutes
ols_threshold = 0.1         # to switch b/w Q and R model output       

def predict(scn, timenow, daytype, cur):
    
    try:
        # Find Last Updated
        query = "SELECT `LastUpdated`, `CurrentFlow` FROM `utmc_transport_link_data_dynamic` WHERE `SystemCodeNumber` = '%s' AND `LastUpdated` >= '%s' AND `Reality` = 'Detector' ORDER BY `LastUpdated` DESC" %(scn, str(timenow - datetime.timedelta(minutes = granularity)))
        cur.execute(query)
        # print query
        result = cur.fetchone()
        
        # If not updated then return
        if result == None:
            print(str(timenow) + ": Flow for " + scn + " is not updated.")  
            return
        
        lastflow_a = float(result[1])
        lasttime   = result[0]
        print(str(lasttime) + "     " + str(lastflow_a))

        # Getting last-time and previous time in 5-mins form
        lasttime_r = lasttime - datetime.timedelta(seconds = lasttime.second)  
        rem        = (lasttime_r.minute) % 5
        lasttime_r = lasttime_r - datetime.timedelta(minutes = rem)
        #prev_t     = lasttime_r - datetime.timedelta(minutes = 5)
        
        # OUTLIER CLASSIFIER--Classify whether last updated flow was an outllier or not
        # Sets FlowStatus_TypeID to 1 if the flow was an outlier        
        
        if lastflow_a != None and outlier_classifier(lasttime, scn, lastflow_a, cur):      
            query = "UPDATE `utmc_transport_link_data_dynamic` SET `LinkStatus_TypeID` = '%d' WHERE `LastUpdated` = '%s' AND `SystemCodeNumber` = '%s' AND `Reality` = 'Detector'" %(1, str(lasttime), scn)
            # print (query)
            cur.execute(query)
                
        # CREATING FEATURE VECTOR       
        # Gets last 6 recorded flows    
        arr = Single_Vector(lasttime, tlag, granularity, scn, cur)    

        # Getting historic values  
        arr_h = []
        
        for ahead in time_aheads:
            timenew = lasttime_r + datetime.timedelta(minutes = ahead)
            if timenew.day - lasttime_r.day == 0:                               # daytype changes for timenew around 11 PM prediction
                arr_h.append(historic_profile(timenew, scn, daytype, cur))
            if abs(timenew.day - lasttime_r.day) > 0:                           # used abs function for month end 
                if daytype < 7:
                    arr_h.append(historic_profile(timenew, scn, daytype+1, cur))
                else:
                    arr_h.append(historic_profile(timenew, scn, 1, cur))
                    
        # Getting e_5 values to update tis_detector_features table
        query = "SELECT `PredictionId`, `CurrentFlow`, `LastUpdated` FROM `utmc_transport_link_prediction` WHERE `SystemCodeNumber` = '%s' AND `DayTypeId` = %d" %(scn, daytype)
        # print(query)
        cur.execute(query)
        pred_s = cur.fetchone()

        if pred_s == None:
            #new_pred_id = scn + '_' + str(daytype).zfill(2)
            new_pred_id = scn + '_' + str(daytype)
            arr = np.append(arr,np.nan)                       
            
        else:
            new_pred_id = pred_s[0]
            
            #if (((lasttime - pred_s[2]).seconds) <= (granularity*60)):   
            if (((lasttime - pred_s[2]).seconds) <= ((granularity*60)+60)):   
                arr = np.append(arr, pred_s[1] - lastflow_a)     
                
            else:
                arr = np.append(arr, np.nan)

        # print("Update Feature Table..")        
        update_feature_table(arr.astype(float), tlag, lasttime_r, scn, arr_h, time_aheads, cur)   
        
        # Removing unnecessary values in arr
        arr = arr[:-2]
        
        for ahead_count in range(len(time_aheads)):
            ahead = time_aheads[ahead_count]
            endtime = lasttime_r + datetime.timedelta(minutes = ahead)

            # New Vector contains arr_h and arr 
            new_vector = [arr_h[ahead_count]]
            new_vector.extend(arr)
            # print(new_vector)
            
            # If any element is nan then pass and ignore prediction
            if np.any(np.isnan(new_vector)):
                continue
            
            neigh_data = find_neighbours(new_vector, tnn, scn, ahead, cur, lasttime_r)
            neigh_data = np.array(neigh_data)
            # print(np.shape(neigh_data))

            if np.shape(neigh_data)[0] == 0:
                continue
            if ((tnn < 30) or (np.shape(neigh_data)[0] < 30)) :
                y_pred = np.mean(neigh_data[:,-1])
            else:
                y_pred = QR_train(neigh_data, new_vector)
             
            # OLS regression on errors only for time_ahead = 5 min
            # print("OLS Starts Here.")
            if ahead_count == 0:
                ols_v, ols_type = OLS_Vector(endtime, tlag, scn, time_aheads[ahead_count], granularity, cur)
                query = "SELECT * FROM `tis_transport_link_ols_params` WHERE `SystemCodeNumber` = '%s' AND `OLS_type` = '%s'" %(scn, ols_type)
                cur.execute(query)
                ols_params = cur.fetchone()

                if ols_params == None:
                    pass
                else:
                    ols_params = ols_params[1:-1]
                    err_part   = ols_params[0]
                    for err_count in range(1,len(ols_params)):
                        if ols_params[err_count] == None:
                            break
                        err_part = err_part + (ols_params[err_count]*ols_v[err_count - 1])
                    
                    # Ignoring err_part if the ratio abs(err_part)/y_pred is greater than threshold
                    if y_pred > 0:
                        if abs(err_part)/y_pred < ols_threshold:
                            y_pred = y_pred - err_part                 
                        else:
                            y_pred = y_pred
                    else:
                        y_pred = 0
                                
            if ahead_count == 0:

                if pred_s == None:      
                    query = "INSERT INTO `utmc_transport_link_prediction`(`PredictionId`,`SystemCodeNumber`, `CurrentFlow`, `LastUpdated`, `StartTime`, `EndTime`, `DayTypeId`) VALUES ('%s','%s',%f,'%s','%s','%s','%s')" %(new_pred_id, scn, y_pred, str(lasttime), str(lasttime_r.time()), str((endtime).time()), str(daytype))
                else:
                    query = "UPDATE `utmc_transport_link_prediction` SET `CurrentFlow`=%f,`LastUpdated`='%s',`StartTime`='%s',`EndTime`='%s' WHERE `PredictionId` = '%s'" %(y_pred, str(lasttime), str(lasttime_r.time()), str((endtime).time()), new_pred_id)
                # print query
                cur.execute(query)
                print('Updated - utmc_detector_prediction ' + str(endtime) + ' ' + scn)
            
            query = "SELECT `CurrentFlow` FROM `utmc_transport_link_prediction_data` WHERE `SystemCodeNumber` = '%s' AND `StartTime` = '%s'" %(scn, str(endtime+datetime.timedelta(minutes = -1*granularity)))
            # print(query)
            cur.execute(query)
            rowexits = cur.fetchall()

            if len(rowexits) == 0:
                query = "INSERT INTO `utmc_transport_link_prediction_data`(`PredictionID`, `SystemCodeNumber`, `StartTime`, `EndTime`, `CurrentFlow`, `Occupancy`, `Headway`) VALUES ('%s','%s','%s','%s',%f,0,0)" %(new_pred_id, scn, str(endtime + datetime.timedelta(minutes = -1*granularity)), str(endtime), y_pred)
            else:
                query = "UPDATE `utmc_transport_link_prediction_data` SET `CurrentFlow` = %f WHERE `SystemCodeNumber` = '%s' AND `StartTime` = '%s'" %(y_pred, scn, str(endtime + datetime.timedelta(minutes = -1*granularity)))
            # print(query)
            cur.execute(query)
            # print('Updated - utmc_detector_prediction_data for time ahead: ' + str(ahead))
            
    except Exception as e:
        print("-----------------------")
        print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)
        print(e)
        print("-----------------------")
    
def Predict_for_(pred_time):
    
    con = Connect()
    cur = con.cur
    
    query = "SELECT `SystemCodeNumber` FROM `utmc_transport_link_static`"
    cur.execute(query)
    SCN = cur.fetchall()
    
    # Getting day-type according to our database
    days_id = {}
    query = "SELECT `TypeID`, `TypeDescription` FROM `utmc_day_typeid` WHERE 1"     
    cur.execute(query)
    result = cur.fetchall()
    for typ in result:
        days_id[typ[1]] = typ[0]
    dow = pred_time.weekday()                                      
    type_desc = calendar.day_name[dow]
    daytype = days_id[type_desc]   
    
    # Detecting for all sensor                            
    for i in range(len(SCN)):

        temp_link_scn = SCN[i][0]
        temp_link_scn = "LINK002"
        predict(temp_link_scn, pred_time, daytype, cur)
        sys.exit(0)
        
    # Uncomment below to predict for one sensor 
    '''
    scn = '..Fill SCN here..'
    print("----------------------------------------------------------------")
    print("Predicting at %s and for sensor %s "%(pred_time, SCN[i][0]))       
    predict(scn, pred_time, daytype, cur)
    print("Prediction Done at %s and for sensor %s "%(pred_time, SCN[i][0]))
    print("----------------------------------------------------------------")
    # Done with Prediction'''
    
    print('Prediction Finished')
    
    con.close()

if __name__=='__main__':
    
    # Predicting for current time
    pred_time = datetime.datetime.now()
    
    #Uncoment to predict for specific time
    pred_time = datetime.datetime.strptime("2020-01-06 15:00:02","%Y-%m-%d %H:%M:%S")
    
    Predict_for_(pred_time)                