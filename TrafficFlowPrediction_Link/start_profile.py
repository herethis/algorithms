from connect_2_db import Connect
from profile_table import update_profile_table

granularity = 5.0    # in minutes
prof_hist = 30        # in days

def profile_updation():

    con = Connect()
    cur = con.cur
    
    query = "SELECT DISTINCT `SystemCodeNumber` FROM `utmc_transport_link_data_dynamic` WHERE `Reality` = 'Detector'"
    cur.execute(query)
    SCN = cur.fetchall()

    for scn in SCN:
        scn = scn[0]
        update_profile_table(scn, prof_hist, granularity, cur)
        
    con.close()
        
if __name__=='__main__':
    
    # Updates historic profile table 
    profile_updation()
