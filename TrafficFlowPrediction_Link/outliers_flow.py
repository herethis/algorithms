# Returns True OR False on the basis of ABS MED DEV if the flow is outlier or not 
import numpy as np
import datetime

def isOutlier(data, cut):
    
    med = np.median(data)
    mad = 1.4826 * np.median(abs(data - med))
    if mad < 1e-9:
        return False
    eps = abs(data - med) / mad
    # print mad, eps[-1]
    #print eps
    if eps[-1] > cut:
        return True
    
    return False

def flow_merge(flow1, flow2):
    memo = {}
    flows = []
    for f in flow1:
        if f[1] == None:
            continue
        t = f[0]
        t = t - datetime.timedelta(seconds = t.second)
        rem = (t.minute)%5
        t = t - datetime.timedelta(minutes = rem)
        memo[t] = float(f[1])

    for f in flow2:
        if f[1] == None:
            continue
        t = f[0]
        t = t - datetime.timedelta(seconds = t.second)
        rem = (t.minute)%5
        t = t - datetime.timedelta(minutes = rem)
        if t in memo:
            flows.append(memo[t] + float(f[1]))

    return flows

def lastdays(lasttime, duration, links, cur):
    
    # Sample query: SELECT `TotalFlow` FROM `utmc_detector_dynamic` WHERE ((`LastUpdated` >= '2019-03-08 08:00:00' AND `LastUpdated` < '2019-03-08 09:00:00') OR (DATE(`LastUpdated`) >= '2019-03-01' AND DATE(`LastUpdated`) < '2019-03-07' AND TIME(`LastUpdated`) >= '08:30:00' AND TIME(`LastUpdated`) < '09:30:00')) AND `SystemCodeNumber` = 'J000111'
    flows = []
    up    = lasttime        
    
    down = lasttime + datetime.timedelta(minutes = -60)

    up1 = lasttime + datetime.timedelta(days = -1, minutes = 30)
    down1 = lasttime + datetime.timedelta(days = -7, minutes = -30)
    
    query = "SELECT `CurrentFlow` FROM `utmc_transport_link_data_dynamic` WHERE ((`LastUpdated` >= '%s' AND `LastUpdated` < '%s') OR (DATE(`LastUpdated`) >= '%s' AND DATE(`LastUpdated`) < '%s' AND TIME(`LastUpdated`) >= '%s' AND TIME(`LastUpdated`) < '%s')) AND `SystemCodeNumber` = '%s' AND `Reality` = 'Detector'" %(str(down), str(up), str(down1.date()), str(up1.date()), str(down1.time()), str(up1.time()), links)
    # print query
    cur.execute(query)
    flows = cur.fetchall()

    return flows

def outlier_classifier(lasttime, scn, flow, cur):

    duration = 7
    data = lastdays(lasttime, duration, scn, cur)
    
    data = np.array(data)[:]
    data = np.append(data, flow)
     
    cut = 2.0
    if lasttime.hour > 6 and lasttime.hour < 22:
        cut = 3.5
    
    return isOutlier(data, cut)
