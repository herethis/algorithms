import datetime
import matplotlib.pyplot as plt

from connect_db import Con

def Scatter_Plot(Act_Flows, Pred_Flows):
    
    x = Act_Flows
    y = Pred_Flows
    # Formalities

    # Setting aspect ratio
    fig = plt.figure()
    ax  = fig.add_subplot()
    ax.set_aspect('equal')
    
    plt.title('Predicted Flow vs. Actual Flow')
    plt.xlabel('Actual Flow') 
    plt.ylabel('Predicted Flow') 
    
    # Setiing x and y-axis limit
    lim = 300
    plt.xlim(0, lim)
    plt.ylim(0, lim)
    # Plotting x = y axis
    x1 = [0, lim]
    y1 = [0, lim]
    plt.plot(x1,y1, color='black')
    # Plotting actual and predicted flow values
    plt.scatter(x, y, marker='.', c='red', )
    plt.show()
    
def Time_Plot(Act_Flows, Pred_Flows, Time_Stamps):
    
    x = Act_Flows
    y = Pred_Flows
    z = Time_Stamps
    # Formalities
    plt.title('Time-series plot')
    plt.xlabel('Time') 
    plt.ylabel('Flow') 
    
    plt.plot(z, x, color='green', linestyle='dashed', linewidth = 1, 
         marker='o', markerfacecolor='blue', markersize=2) 

    plt.plot(z, y, color='red', linestyle='dashed', linewidth = 1, 
         marker='o', markerfacecolor='red', markersize=2) 
    
    plt.show()
    
def Fetch_Data(starttime, num_of_pred, scn, cur):    
    
    pred_time = starttime
        
    act_list  = []
    pred_list = []
    time_list = []
    
    count    = 0    #count valid flows
    a_count  = 0    #count zero actual-flows
    o_count  = 0    #outliers count
        
    for i in range(num_of_pred):   
            
        # Getting Predicted FLow
        query = "SELECT `TotalFlow` FROM `utmc_detector_prediction_data` WHERE `SystemCodeNumber`='%s' AND `EndTime` = '%s'"%(scn, str(pred_time))
        cur.execute(query)
        data_pred = cur.fetchone()
            
        # Getting Actual FLow
        query = "SELECT `t_5` FROM `tis_detector_features` WHERE `SystemCodeNumber`='%s' AND `StartTime` = '%s'"%(scn,str(pred_time + datetime.timedelta(minutes=-5)))
        cur.execute(query)
        data_act  = cur.fetchone()
            
        # Getting outlier
        query = "SELECT FlowStatus_TypeID FROM `utmc_detector_dynamic` WHERE `SystemCodeNumber`='%s' AND `LastUpdated` > '%s' AND `LastUpdated` < '%s'"%(scn,str(pred_time+datetime.timedelta(minutes=-5)), str(pred_time))
        cur.execute(query)                                                                                                                                      #pred_time+datetime.timedelta(minutes=-5)
        data_type  = cur.fetchone()
            
        if data_type != None:
            flow_type = data_type[0]
        else:
            flow_type = 0 # Assuming not-outlier if no entries found
                
        if data_act != None and data_pred != None:
            
            if data_act[0] != 0 and flow_type == 0:
                
                act_list.append(data_act[0])
                pred_list.append(data_pred[0])
                time_list.append(pred_time)

                count += 1
                    
            elif flow_type == 1:
                #print('Outlier detected - Ignoring prediction')
                o_count += 1
            else:
                #print('zero actual flow value')
                a_count += 1
        
        pred_time = pred_time + datetime.timedelta(minutes=5)
        
    return act_list, pred_list, time_list
        
if __name__=='__main__':
    
    con = Con()
    cur = con.cur
    
    # Specify below values to get scatter and time-series plots
    starttime   = datetime.datetime.strptime("2019-11-26 09:00:00","%Y-%m-%d %H:%M:%S")
    scn    = 'J008_L01_02'
    n_hour =  10
    # ---------------------------------------------------------------------------------

    num_of_pred = int(n_hour*12)
    if num_of_pred > 0:
        
        Act_Flows, Pred_Flows, Time_Stamps = Fetch_Data(starttime, num_of_pred, scn, cur)

        print('Scatter Plot : \n')
        Scatter_Plot(Act_Flows, Pred_Flows)
        print('Time-Series Plot : \n')
        Time_Plot(Act_Flows, Pred_Flows, Time_Stamps)
    
    else:
        print('Enter time limits correctly')
        
    