# Tests Short Term Travel Time Prediction Model 

import datetime
from connect_db import Con

if __name__=='__main__':
    
    # Fill start-time and number of hours to test the model
    starttime = datetime.datetime.strptime("2019-11-29 09:00:00","%Y-%m-%d %H:%M:%S")
    n_hour    = 4
    
    num_of_pred = int(n_hour*60/5)
        
    print('Start-time       : ', starttime)
    print('Prediction count : ', num_of_pred)

    con = Con()
    cur = con.cur
    
    query = 'SELECT DISTINCT(`SystemCodeNumber`) FROM `tis_detector_features` ORDER BY `tis_detector_features`.`SystemCodeNumber` ASC'
    cur.execute(query)
    SCN = cur.fetchall()
   
    for i in range(len(SCN)):
        
        scn = SCN[i][0]
        

        
        pred_time = starttime
        
        pe_list  = []
        ape_list = []
        count    = 0    #count valid flows
        a_count  = 0    #count zero actual-flows
        o_count  = 0    #outliers count
        
        for i in range(num_of_pred):   # this determines timesteps
            
            # Getting Predicted FLow
            query = "SELECT `TotalFlow` FROM `utmc_detector_prediction_data` WHERE `SystemCodeNumber`='%s' AND `EndTime` = '%s'"%(scn, str(pred_time))
            cur.execute(query)
            data_pred = cur.fetchone()
            
            # Getting Actual FLow
            query = "SELECT `t_5` FROM `tis_detector_features` WHERE `SystemCodeNumber`='%s' AND `StartTime` = '%s'"%(scn,str(pred_time + datetime.timedelta(minutes=-5)))
            cur.execute(query)
            data_act  = cur.fetchone()
            
            # Getting outlier
            query = "SELECT FlowStatus_TypeID FROM `utmc_detector_dynamic` WHERE `SystemCodeNumber`='%s' AND `LastUpdated` > '%s' AND `LastUpdated` < '%s'"%(scn,str(pred_time+datetime.timedelta(minutes=-5)), str(pred_time))
            cur.execute(query)                                                                                                                                      #pred_time+datetime.timedelta(minutes=-5)
            data_type  = cur.fetchone()
            
            if data_type != None:
                flow_type = data_type[0]
            else:
                flow_type = 0 # Assuming not-outlier if no entries found
                
            if data_act != None and data_pred != None:
                if data_act[0] != 0 and flow_type == 0:
                    err   = data_pred[0]-data_act[0]
                    pe = 1.0*(err)/data_act[0]
                    ape = abs(pe)
                    
                    pe_list.append(pe)
                    ape_list.append(ape)
                    count += 1
                    
                elif flow_type == 1:
                    #print('Outlier detected - Ignoring prediction')
                    o_count += 1
                else:
                    a_count += 1
            pred_time = pred_time + datetime.timedelta(minutes=5)
        
        print('..............................................')
        print('For Detector :',scn)

        if count!=0:
            print('MPE  : ', 100.0*sum(pe_list)/count)
            print('MAPE : ', 100.0*sum(ape_list)/count)

        print('Accepted fows count %d & Rejected flows count %d'%(count, a_count))
        print('Outliers count :', o_count)
        print('..............................................')
        
        
       
            
            