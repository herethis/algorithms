from connect_2_db import Connect
import numpy as np
# from con import Con
import datetime

hard_down = 0
hard_up = 2000

# def take_out_outlier(newtime, link, new_flow):
# 	# print str(newtime.time())
# 	con = Con()
# 	cur = con.cur
# 	if str(newtime.time()) < "00:05:00":
# 		newtime = newtime - datetime.timedelta(minutes = newtime.minute%5)
# 		newtime = newtime - datetime.timedelta(seconds = (newtime.second+1))
# 	query = "SELECT `TotalFlow` FROM `utmc_detector_dynamic` WHERE `LastUpdated` < '%s' AND TIME(`LastUpdated`) < '%s' AND TIME(`LastUpdated`) >= '%s' AND `SystemCodeNumber` = '%s'" %(str(newtime), str(newtime.time()), str((newtime + datetime.timedelta(minutes = -1*granularity)).time()), link)
# 	print query
# 	# sys.exit(0)
# 	cur.execute(query)
# 	flow_pop = cur.fetchall()
# 	flow_pop = np.array(flow_pop).reshape(1,-1)[0]
# 	dow = newtime.weekday()
# 	type_desc = calendar.day_name[dow]
# 	query = "SELECT `TypeID` FROM `utmc_day_typeid` WHERE `TypeDescription` = '%s'" %(type_desc)
# 	cur.execute(query)
# 	daytype = cur.fetchone()[0]
# 	con.close()
# 	median = historic_profile(newtime + datetime.timedelta(minutes = -1*granularity), link, daytype)
# 	new_flow = 0
# 	median = np.median(flow_pop)
# 	# print median, flow_pop, new_flow
# 	meanval = np.median(abs(flow_pop - median))
# 	# print meanval
# 	zscore = 0.6745*abs(median - new_flow)/meanval
# 	print zscore
# 	q25 = np.percentile(flow_pop, 25)
# 	q75 = np.percentile(flow_pop, 75)
# 	print q75, q25
# 	print q25 - (1.5*(q75 - q25))
# 	return zscore

def give_outlier_bounds(data, time, hard = False):
	if hard:
		data = data[(data > hard_down) & (data < hard_up)]
	# print data
	if len(data) == 0:
		return hard_down, hard_up
	q25 = np.percentile(data, 25)
	q75 = np.percentile(data, 75)
	# print q75, q25
	up = q75 + (1.5*(q75 - q25))
	down = q25 - (1.5*(q75 - q25))
	if len(data) == 1:
		up = up + 5
		down = down - 5		
	if hard:
		if up > hard_up:
			up = hard_up
		if down < hard_down:
			down = hard_down
	return down, up
