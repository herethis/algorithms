import datetime
import numpy as np
from outlier_removal import give_outlier_bounds

def update_profile_table(link, prof_hist, granularity, cur):

    # query = "DELETE FROM `utmc_detector_profile_data` WHERE `SystemCodeNumber` = '%s'" %(link)
    # cur.execute(query)
    start = datetime.datetime.strptime("00:00:00", "%H:%M:%S")
    # end = datetime.datetime.strptime("23:59:59", "%H:%M:%S")
    # print start, end
    total = 24*60/5
    query = "SELECT `LastUpdated` FROM `utmc_detector_dynamic` WHERE 1 ORDER BY `LastUpdated` DESC LIMIT 1"
    # print query
    cur.execute(query)
    endtime   = cur.fetchone()[0]
    #endtime = datetime.datetime.strptime("2020-01-08 12:00:00", "%Y-%m-%d %H:%M:%S")
    starttime = endtime + datetime.timedelta(days = -prof_hist)

    query = "SELECT `TypeID` FROM `utmc_day_typeid` WHERE 1"
    cur.execute(query)
    typeid_all = cur.fetchall()
    
    up = 0
    down = 0
    flow_status_id = 0
    
    for typeid in typeid_all:
        
        typeid = typeid[0]
        query = "SELECT `ProfileId` FROM `utmc_detector_profile` WHERE `DayTypeId` = '%d' AND `ProfileId` LIKE '%%%s%%'" %(typeid,link)
        cur.execute(query)
        profid = cur.fetchone()

        if profid == None:
            profid = link + '_' + str(typeid)
            query = "INSERT INTO `utmc_detector_profile`(`ProfileId`, `DayTypeId`) VALUES ('%s',%d)" %(profid, typeid)
            cur.execute(query)
        else:
            profid = profid[0]

        # if profid[-1] != '1':
        #     continue
        newtime = start
        print("Loading data for: " + profid)
        for tim_win in range(total):
            newtime = newtime + datetime.timedelta(minutes = granularity)
            # print("========", newtime.time())
            # if str(newtime.time()) == "00:00:00":
            query = "SELECT `TotalFlow` FROM `utmc_detector_dynamic` WHERE `LastUpdated` < '%s' AND TIME(`LastUpdated`) <= '%s' AND TIME(`LastUpdated`) >= '%s' AND `SystemCodeNumber` = '%s' AND DAYOFWEEK(`LastUpdated`) = %d AND `FlowStatus_TypeID` = '%d'" %(str(endtime), str((newtime + datetime.timedelta(minutes = 1*granularity, seconds = -1)).time()), str((newtime).time()), link, typeid, flow_status_id)
            query = "SELECT `TotalFlow` FROM `utmc_detector_dynamic` WHERE `LastUpdated` < '%s' AND TIME(`LastUpdated`) <= '%s' AND TIME(`LastUpdated`) >= '%s' AND `SystemCodeNumber` = '%s' AND `FlowStatus_TypeID` = '%d'" %(str(endtime), str((newtime + datetime.timedelta(minutes = 1*granularity, seconds = -1)).time()), str((newtime).time()), link, flow_status_id)
            # print query
            # sys.exit(0)
            cur.execute(query)
            temp_prof = cur.fetchall()
            #print len(temp_prof)
            # print(temp_prof)
            query = "SELECT `TotalFlow` FROM `utmc_detector_profile_data` WHERE `ProfileID` = '%s' AND `StartTime` = '%s'" %(profid, str((newtime - datetime.timedelta(minutes = granularity)).time()))
            cur.execute(query)
            res = cur.fetchone()
            # print res
            if len(temp_prof) == 0 and res == None:
                print("No data for: " + profid + str(newtime))
                th = 0
            elif len(temp_prof) == 0:
                print("No data for: " + profid + str(newtime))
                continue
            else:
                temp_prof = np.array(temp_prof).reshape(-1)
                down, up = give_outlier_bounds(temp_prof, newtime, True)
                # print down, up
                # print temp_prof
                temp_prof = temp_prof[((temp_prof > down) & (temp_prof < up))]
                if len(temp_prof) == 0 and res == None:
                    th = 0
                    up = 0
                    down = 0
                elif len(temp_prof) == 0:
                    continue
                else:
                    th = np.median(temp_prof)
                    down = np.min(temp_prof)
                    up = np.max(temp_prof)
                #print th
            # sys.exit(0)
            if res == None:
                query = "INSERT INTO `utmc_detector_profile_data`(`ProfileID`, `SystemCodeNumber`, `StartTime`, `EndTime`, `TotalFlow`, `FlowInterval`, `UpperBound`, `LowerBound`) VALUES ('%s','%s','%s','%s','%s',%d, %d, %d)" %(profid, link, str((newtime - datetime.timedelta(minutes = granularity)).time()), str(newtime.time()), str(th), granularity, up, down)
            else:
                query = "UPDATE `utmc_detector_profile_data` SET `TotalFlow` = %s, `UpperBound` = %s, `LowerBound` = %s WHERE `ProfileID` = '%s' AND `StartTime` = '%s'" %(str(th), str(up), str(down), profid, str((newtime - datetime.timedelta(minutes = granularity)).time()))
            cur.execute(query)
    
def historic_profile(timenew, scn, typeid, cur):
    
    query = "SELECT `ProfileId` FROM `utmc_detector_profile` WHERE `DayTypeId` = '%d' AND `ProfileId` LIKE '%%%s%%' " %(typeid, scn)
    cur.execute(query)
    # print(query)
    profid = cur.fetchone()
    
    if profid == None:
        return np.nan
    profid = profid[0]
    
    timenew = timenew.time()
    query = "SELECT `TotalFlow` FROM `utmc_detector_profile_data` WHERE `ProfileID` = '%s' AND `EndTime` = '%s'" %(profid, str(timenew)) 
    # print(query)
    cur.execute(query)
    
    th = cur.fetchone()
    if th == None:
        return np.nan
    else:
        th = th[0]

    return th
