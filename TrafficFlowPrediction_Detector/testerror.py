from connect_2_db import Connect
import datetime
import numpy as np

def finderrors(link, last_hours = 24):
	con = Connect()
	cur = con.cur
	query = "SELECT `StartTime` FROM `tis_detector_features` WHERE `SystemCodeNumber` = '%s' ORDER BY `StartTime` DESC" %(link)
	cur.execute(query)
	lasttime = cur.fetchone()
	if lasttime == None:
		print("No predicted values for: " + link)
		return
	lasttime = lasttime[0]
	query = "SELECT AVG(ABS(`e_5`)/`t_5`) FROM `tis_detector_features` WHERE `SystemCodeNumber` = '%s' AND `t_5` is NOT NULL AND `e_5` is NOT NULL AND `t_5` != 0 AND `StartTime` >= '%s' AND TIME(`StartTime`) >= '06:00:00' AND TIME(`StartTime`) <= '22:00:00'" %(link, str(lasttime - datetime.timedelta(hours = last_hours)))
	cur.execute(query)
	res = cur.fetchall()
	print link, res[0][0]
	con.close()
	return res[0][0]

if __name__ == "__main__":
	con = Connect()
	cur = con.cur
	query = "SELECT DISTINCT `SystemCodeNumber` FROM `tis_detector_features` WHERE 1"
	cur.execute(query)
	scns = cur.fetchall()
	con.close()

	err = []

	for scn in scns:
		scn = scn[0]
		temp = finderrors(scn, 12)
		if temp == None:
			pass
		else:
			err.append(temp)

	print "Total: ", np.mean(err)
