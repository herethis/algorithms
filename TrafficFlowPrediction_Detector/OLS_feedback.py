import statsmodels.formula.api as sm
import numpy as np
import datetime

granularity = 5.0         # in minutes
ols_hist    = 2           # in days
time_aheads = [5, 15, 60] # attention

def OLS_Vector(timenew, tlag, scn, ahead, granularity, cur):
    
    arr = []
    olstype = ''
    time_up   = timenew + datetime.timedelta(minutes = -1 * granularity)
    time_down = timenew + datetime.timedelta(minutes = -1 * granularity * tlag)
    
    query = "SELECT `e_" + str(ahead) + "`, `StartTime` FROM `tis_transport_link_features` WHERE `SystemCodeNumber` = '%s' AND `StartTime` >= '%s' AND `StartTime` <= '%s' ORDER BY `StartTime` DESC" %(scn, str(time_down), str(time_up))
    # print query
    cur.execute(query)

    result = cur.fetchall()
    
    if len(result):
        ind = 0
        for i in range(tlag):
            timenew = timenew + datetime.timedelta(minutes = -granularity)
            if ind < len(result) and result[ind][1] >= timenew and result[ind][0] != None:
                arr.append(result[ind][0])
                olstype = olstype + '1'    
            else:
                olstype = olstype + '0'
            ind += 1
    else:
        olstype = tlag * '0'

    return arr, olstype

def OLS_train_dataset(endtime, tlag, scn, olstype, granularity, time_aheads, ols_hist, cur):
                     
    starttime = endtime + datetime.timedelta(days = -1*ols_hist)
    query = "SELECT `StartTime`, `e_5` FROM `tis_transport_link_features` WHERE `StartTime` <= '%s' AND `StartTime` >= '%s' AND `SystemCodeNumber` = '%s' " %(endtime, starttime, scn)
    cur.execute(query)
    data = cur.fetchall()
    
    dataset_x = []
    dataset_y = []

    for d_count in range(len(data)):
        if data[d_count][1] == None:
            pass
        else:
            arr, olstype_temp = OLS_Vector(data[d_count][0], tlag, scn, time_aheads[0], granularity, cur)
            
            if olstype == olstype_temp:
                dataset_x.append(arr)
                dataset_y.append(data[d_count][1])

    return dataset_x, dataset_y

def OLS_train(endtime, tlag, olstype, scn, granularity, time_aheads, ols_hist, cur):
    
    data_x, data_y = OLS_train_dataset(endtime, tlag, scn, olstype, granularity, time_aheads, ols_hist, cur)

    if len(data_y) == 0:
        print(len(data_y))
        return []
    
    eqn = "e0 ~ "
    df = {}
    df["e0"] = data_y
    data_x = np.array(data_x)
    
    size = np.shape(data_x)
    
    for count in range(1, size[1]):
        
        eqn = eqn + "e" + str(count) + " + "
        df["e" + str(count)] = data_x[:,count-1]
    
    eqn = eqn + "e" + str(size[1])
    df["e" + str(size[1])] = data_x[:,size[1] - 1]

    result = sm.ols(formula=eqn, data=df).fit()

    return result.params

def update_OLS_table(scn, drop, tlag, granularity, time_aheads, ols_hist, cur):

    if drop == True:
    
        query = "DROP TABLE `tis_transport_link_ols_params`"
        cur.execute(query)
        
        # Creating new table
        query = "CREATE TABLE `tis_transport_link_ols_params` ( `SystemCodeNumber` CHAR(32) NOT NULL , `Intercept` FLOAT NULL DEFAULT NULL,"
        for lag_count in range(tlag):
            query = query + "`p" + str(lag_count+1) + "` FLOAT NULL DEFAULT NULL,"
        query = query + "  `OLS_type` CHAR(32) NULL DEFAULT NULL)"
        cur.execute(query)

    query = "DELETE FROM `tis_transport_link_ols_params` WHERE `SystemCodeNumber` = '%s'" %(scn)
    cur.execute(query)
    
    query = "SELECT `LastUpdated` FROM `utmc_transport_link_data_dynamic` ORDER BY `LastUpdated` DESC LIMIT 1"
    cur.execute(query)
    lasttime = cur.fetchone()[0]
    
    ttype = int('0b' + '1'*tlag,2)  # Eqv. to 15 = 1111

    for tp in range(1,ttype+1):
        
        temp_type = str(bin(tp))[2:].zfill(tlag)   # Takes binary value between 0000-1111 i.e. 1 to 15

        ols_params = OLS_train(lasttime, tlag, temp_type, scn, granularity, time_aheads, ols_hist, cur)
        
        if len(ols_params) == 0:
            continue
        
        query = "INSERT INTO `tis_transport_link_ols_params`(`SystemCodeNumber`, `Intercept`,"
        for p_count in range(1,len(ols_params)):
            query = query + " `p" + str(p_count) +"`,"
        query = query + " `OLS_type`) VALUES ('%s',%f," %(scn, ols_params[0])
        for p_count in range(1,len(ols_params)):
            query = query + str(ols_params[p_count]) + ","
        query = query + "'%s')" %(temp_type)

        cur.execute(query)

    return False

'''    
if __name__=='__main__':
    
    from connect_2_db import Connect
    con = Connect()
    cur = con.cur
    granularity = 5.0         # in minutes
    ols_hist    = 2           # in days
    ahead = 5
    tlag = 4
    timenew     = datetime.datetime.strptime("2019-11-27 10:40:00","%Y-%m-%d %H:%M:%S")
    scn         = 'J007_L01_01'
    arr, olstype = OLS_Vector(timenew, tlag, scn, ahead, granularity, cur)
'''
