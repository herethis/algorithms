#from connect_db import Con
import numpy as np
from profile_table import historic_profile
import datetime
import calendar

granularity = 5
history = 6 * 30        # in feature days
time_aheads = [5, 15, 60]

def Single_Vector(timenew, tlag, granularity, scn, cur):  # Parameter list changed

    arr = []
    flow_status_id = 0              # Outlier classification - not considering outliers
    
    temp_time_up   = timenew
    temp_time_down = timenew + datetime.timedelta(minutes = -1 * (granularity * (tlag + 1)))
    
    query = "SELECT `TotalFlow`, `LastUpdated` FROM `utmc_detector_dynamic` WHERE `SystemCodeNumber` = '%s' AND `LastUpdated` <= '%s' AND `LastUpdated` > '%s' AND `FlowStatus_TypeID` = '%d' ORDER BY `LastUpdated` DESC" %(scn, str(temp_time_up), str(temp_time_down), flow_status_id)
    # print(query)
    # query inclusive of uptime always because lasttime and lastupdated are same for prediction
    cur.execute(query)
    result = cur.fetchall()
    # print result
    if len(result) == (tlag + 1):
        arr = np.array(result)[:,0]
    elif len(result) == (tlag + 2):
        arr = (np.array(result)[:,0])[:-1]
    else:
        arr = (tlag + 1) * [np.nan]

    # print(arr)    
    return arr
'''tlag = 2
granularity = 5
timenew = datetime.datetime.strptime("2019-05-23 00:07:38","%Y-%m-%d %H:%M:%S")
temp_time_down = timenew + datetime.timedelta(minutes = -1 * (granularity * (tlag + 1)))
print(temp_time_down)'''

def createFeatures(endtime, tlag, scn, granularity, history, time_aheads, cur, drop = False):
    
    if drop == True:      
        
        query = "DROP TABLE IF EXISTS `tis_detector_features`"
        cur.execute(query)

        query = "CREATE TABLE `tis_detector_features` ( `SystemCodeNumber` CHAR(32) NOT NULL ,  `StartTime` DATETIME NOT NULL,"
        for lag_count in range(1,tlag + 1):
            query = query + "`t" + str(lag_count) + "` FLOAT NULL DEFAULT NULL,"
        for ahead in time_aheads:
            query = query + "`th_" + str(ahead) + "` FLOAT NULL DEFAULT NULL,  `t_" + str(ahead) + "` FLOAT NULL DEFAULT NULL, `e_" + str(ahead) + "` FLOAT NULL DEFAULT NULL,"
        query = query + " `use` INT NOT NULL)"
        print(query)

        cur.execute(query)
    
    # Deleting all entries corresponding to scn
    query = "DELETE FROM `tis_detector_features` WHERE `SystemCodeNumber` = '%s' " %(scn)    
    cur.execute(query)

    # Collect all data corresponding to scn between now and history
    starttime = endtime + datetime.timedelta(days = -history)
    query = "SELECT `LastUpdated`, `TotalFlow` FROM `utmc_detector_dynamic` WHERE `SystemCodeNumber` = '%s' AND `LastUpdated` <= '%s' AND `LastUpdated` >= '%s' ORDER BY `LastUpdated` ASC" %(scn, endtime, starttime)
    cur.execute(query)
    data = cur.fetchall()
    
    for d_count in range(len(data)):
        # print(data[d_count])        
        # Getting timehere in multiples of 5 and day-type
        timehere = data[d_count][0]
        # timehere = timehere - datetime.timedelta(minutes = granularity)
        timehere = timehere - datetime.timedelta(seconds = timehere.second)
        rem = timehere.minute % 5
        timehere = timehere - datetime.timedelta(minutes = rem)
        dow = timehere.weekday()
        type_desc = calendar.day_name[dow]
        query = "SELECT `TypeID` FROM `utmc_day_typeid` WHERE `TypeDescription` = '%s'" %(type_desc)
        cur.execute(query)
        typeid = cur.fetchone()[0]
        
        # Array contains current data(recent exact 5 mins multiple) + 4 lagged values + 1 lagged value also
        arr = Single_Vector(timehere + datetime.timedelta(minutes = 5), tlag + 1, granularity, scn, cur)

        arr_h = []        
        for ahead in time_aheads:
            
            timenew = timehere + datetime.timedelta(minutes = ahead)
            if timenew.day - timehere.day == 0:                               # daytype changes for timenew around 11 PM prediction
                hist = historic_profile(timenew, scn, typeid, cur)
                arr_h.append(hist)
            if abs(timenew.day - timehere.day) > 0:                           # used abs function for month end 
                if typeid < 7:
                    hist = historic_profile(timenew, scn, typeid + 1, cur)
                else:
                    hist = historic_profile(timenew, scn, 1, cur)
                arr_h.append(hist)
            
        
        # arr_h contains historic data for the time that corresponds to 
        # same time for which we are predicting
        
        # Adding np.nan so that e_5 value gets updated with np.nan
        arr1 = np.append(arr, np.nan)
        
        update_feature_table(arr1.astype(float), tlag, timehere, scn, arr_h, time_aheads, cur)      # removed + [np.nan]
    return False

def update_feature_table(arr, tlag, timenew, scn, arr_h, time_aheads, cur):

    query = "SELECT * FROM `tis_detector_features` WHERE `SystemCodeNumber` = '%s' AND `StartTime` = '%s'" %(scn, timenew)
    cur.execute(query)
    result = cur.fetchall()
    
    # If any value in arr is np.nan don't use it for prediction 
    if np.any(np.isnan(list(arr[0:5]))):             
        use = 0
    else:
        use = 1
    # Replacing all nan's with NULL 
    arr_h = np.where(np.isnan(list(arr_h)),'NULL',arr_h)
    arr = np.where(np.isnan(arr),'NULL',arr)
    # print arr
    
    # if len(result) == 0:
    if True:
        ####### QUERY CHECK - Please comment out 
        '''arr = np.array([0,1,2,3,4,0.15])
        tlag = 4
        timenew = datetime.datetime.strptime("2019-03-10 12:00:00","%Y-%m-%d %H:%M:%S")
        scn = 'Sys_Code_Num'
        arr_h = [5,15,60]
        time_aheads = [5, 15, 60]
        use = 1'''
        ######################################
        query = "INSERT INTO `tis_detector_features`(`SystemCodeNumber`, `StartTime`,"
        #if arr[-1] != np.nan:
        #if arr[-1] != 'NULL':
            #query += " `e_5`, "
        query = query + "`t_" + str(time_aheads[0]) + "`,"
        for lag_count in range(1, tlag + 1):
            query = query + " `t" + str(lag_count) + "`,"
        for ahead in time_aheads:
            query = query + " `th_" + str(ahead) + "`,"
        query = query + "`e_5`, `use`) VALUES ('%s','%s'," %(scn, str(timenew))
        #if arr[-1] != np.nan:
        #if arr[-1] != 'NULL':
            #query = query + str(arr[-1]) + ","                       
            #query = query + 'NULL' + ","                       
        for lag_count in range(tlag + 1):
            query = query + str(arr[lag_count]) + ","
        for ahead_count in range(len(time_aheads)):
            query = query + str(arr_h[ahead_count]) + ","
        query = query + "%s,%d)" %(str(arr[-1]), use)
        #SAMPLE QUERY PRINT-- INSERT INTO `tis_detector_features`(`SystemCodeNumber`, `StartTime`, `t0`, `t1`, `t2`, `t3`, `t4`, `th_5`, `th_15`, `th_60`, `use`) VALUES ('Sys_Code_Num','2019-03-10 12:00:00', 0,1,2,3,4,0,15,60,1)
        # print(query)
        cur.execute(query)
    
    # Updating t_5, t_15 and t_60 values
    for ahead in time_aheads[1:]:
        timenew_ahead = timenew - datetime.timedelta(minutes = (ahead))
        #query = "SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; "
        #query = "SELECT * FROM `tis_detector_features` WHERE `SystemCodeNumber`='%s' AND `StartTime`='%s' FOR UPDATE; " %(link, str(timenew_ahead))
        query = "UPDATE `tis_detector_features` SET `t_" + str(ahead) + "` = %s WHERE `SystemCodeNumber`='%s' AND `StartTime`='%s' " %(str(arr[0]), scn, str(timenew_ahead))
        # print query
        #query = query + "SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;"
        # print(query)
        cur.execute(query)
        
    # Adding below line to update e_5 correctly-only executed in case of prediction not feature extraction
    # query = "SELECT * FROM `tis_detector_features` WHERE `SystemCodeNumber`='%s' AND `StartTime`='%s' " %(scn, str(timenew-datetime.timedelta(minutes=5)))
    # cur.execute(query)
    # prev = cur.fetchone()
    
    # if prev == None:
    #     print("Previous time not found to update e_5 value")
    # else:
    #     query = "UPDATE `tis_detector_features` SET `e_5` = %s WHERE `SystemCodeNumber`='%s' AND `StartTime`='%s' " %(str(arr[-1]), scn, str(timenew-datetime.timedelta(minutes=5)))
    #     cur.execute(query)
    
    # print("Successfully Updated feature table for %s and date-time %s"%(scn, timenew))
    
def find_neighbours(new_v, tnn, link, ahead, cur, lasttime_r):
    
    # Please comment out below variables 
    '''new_v = [1, 2, 3, 4, 5, 6]
    tnn = 40
    link = 'J07_01_00'
    ahead = 15
    lasttime_r = 'lastime'
    '''
    # print(new_v)
    query = "SELECT `th_" + str(ahead) + "`"
    for v_count in range(1, len(new_v)):
        query = query + ",`t" + str(v_count) + "` "
    query = query + ",`t_" + str(ahead) + "`"
    query = query + " FROM `tis_detector_features` WHERE `th_" + str(ahead) + "` IS NOT NULL AND `t_" + str(ahead) + "` > 0 AND "
    for v_count in range(1,len(new_v)):
        query = query + "`t" + str(v_count) + "` > 0 AND "
    query = query + "`SystemCodeNumber` = '%s'" %(link) + " AND `StartTime` <= '%s'"%(lasttime_r)
    query =  query + " ORDER BY sqrt(power(`th_" + str(ahead) + "` - %f, 2)" %(new_v[0])
    for v_count in range(1,len(new_v)):
        query = query + " + power(`t" + str(v_count) + "` - "+ str(new_v[v_count]) +", 2) "
    query = query + ") ASC LIMIT %d" %(tnn)
    # print(query)
    
    # SELECT `th_15`,`t0` ,`t1` ,`t2` ,`t3` ,`t4` ,`t_15` FROM `tis_detector_features` WHERE `th_15` IS NOT NULL AND `t_15` > 0 AND `t0` > 0 AND `t1` > 0 AND `t2` > 0 AND `t3` > 0 AND `t4` > 0 AND `SystemCodeNumber` = 'J07_01_00' AND `StartTime` < 'lastime' ORDER BY sqrt(power(`th_15` - 1.000000, 2) + power(`t0` - 2, 2)  + power(`t1` - 3, 2)  + power(`t2` - 4, 2)  + power(`t3` - 5, 2)  + power(`t4` - 6, 2) ) ASC LIMIT 40    
    cur.execute(query)
    data = cur.fetchall()
    
    return data
