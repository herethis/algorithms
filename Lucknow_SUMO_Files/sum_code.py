from createsumofiles_tr import run_trfilecode
import glob
import sys
from con import Con
import threading
import os
import time

sumo_sleep = 7*24*60*60
# group = "GRP007"
# # netFile = group + "/network0.xml"
# file = glob.glob("Temp/*net.xml")
# # print file sys.exit(0)
# newFile = file[0] print newFile
# # sys.exit(0)
# profile = False
def update_offline_sumo():
	threading.Timer(sumo_sleep, update_offline_sumo).start()
	con = Con()
	cur = con.cur
	query = "SELECT DISTINCT `Group_SCN` FROM `calendar` WHERE 1"
	cur.execute(query)
	allgroups = cur.fetchall()
	for group in allgroups:
		group = group[0]
		if not os.path.isdir(str(group)):
			print("Group files does not exist for: ", group)
		else:
			query = "SELECT `TimeTable_SCN`, `Slot_Order`, `Date` FROM `calendar` WHERE `Group_SCN` = '%s'" %(group)
			cur.execute(query)
			allplans = cur.fetchall()
			# print allplans
			print(len(allplans))
			for singleplan in allplans:
				print(singleplan)
				query = "SELECT `StartTime`, `EndTime` FROM `time_tables` WHERE `SlotOrder` = %s AND `TimeTableSCN` = '%s'" %(singleplan[1], singleplan[0])
				print(query)
				cur.execute(query)
				timeslots = cur.fetchone()
				print(timeslots)
				print(group + "/" + singleplan[2] + "/" + '_'.join(timeslots))
				if not os.path.isdir(group + "/" + singleplan[2] + "/" + '_'.join(timeslots)):
					os.makedirs(group + "/" + singleplan[2] + "/" + '_'.join(timeslots))
					print("Group files does not exist for: ", group + "/" + singleplan[2] + "/" + '_'.join(timeslots))
					# print "NOOOOOOOOO"
				else:
					newFile = str(group) + "/network0.xml"
					run_trfilecode(group, newFile, profile, singleplan[2], '_'.join(timeslots))
		# sys.exit(0)
	con.close()
# update_offline_sumo()
while True:
	con = Con()
	cur = con.cur
	query = "SELECT DISTINCT `Group_SCN` FROM `calendar` WHERE 1"
	cur.execute(query)
	allgroups = cur.fetchall()
	con.close()
	# print allgroups
	for group in allgroups:
		group = group[0]
		#group = "GRP003"
		print group
		if not os.path.isdir('/home/atcsadmin/Lucknow_SUMO_Files/'+str(group) + "/" + "Online"):
			print("Group files does not exist for: " + str(group))
		else:
			print("GROUP", group)
			newFile = '/home/atcsadmin/Lucknow_SUMO_Files/'+str(group) + "/network0.xml"
			run_trfilecode(group, newFile, False)
			#time.sleep(60*60)
	sys.exit(0)

