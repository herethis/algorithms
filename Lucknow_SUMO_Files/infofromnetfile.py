import xml.etree.ElementTree as etree
import sys
import numpy as np
from con import Con
import pprint

con = Con()
cur = con.cur

netfile = "GRP032/network0.xml"
# netfile = "JaipurHandDrawn.net.xml"

tree = etree.parse(netfile)
root = tree.getroot()

# print root


# for ch in root.findall("connection"):
# 	elem = []

# sys.exit(0)
# new = {'from': [], 'to': [], 'linkIndex': [], 'dir': []}

state = {}
links = {}
# dirs = {}

for ch in root.findall("connection"):
	if 'tl' in (ch.attrib).keys():
		# print ch.tag
		# if ch.attrib['tl'] == 'J015':
			# if ch.attrib['from'] == 'J015_L01':
				# print ch.attrib
		# print ch.attrib
		ji = ch.attrib['tl']
		fr = ch.attrib['from']
		query = "SELECT `from_link` FROM `utmc_signal_movements` WHERE `DET_SCN` LIKE '%%%s%%'" %(fr)
		print query
		cur.execute(query)
		temp_links_tab = cur.fetchall()
		print temp_links_tab
		if len(temp_links_tab) == 0:
			change_det = int(ch.attrib['from'].split('_')[1][1:])
		else:
			change_det = temp_links_tab[0][0]
			for temp_tab in temp_links_tab:
				if change_det == temp_tab[0]:
					pass
				else:
					print "Something wrong with DET_SCN"
					sys.exit(0)
		fr = str(change_det)

		# print ji, fr
		if ji not in state.keys():
			state[ji] = ''
			links[ji] = {}
			# dirs[ji] = {}
		if str(fr) not in links[ji].keys():
			links[ji][str(fr)] = {}
		state[ji] = state[ji] + 'r'
		# print links
		if ch.attrib['dir'] not in links[ji][str(fr)].keys():
			links[ji][str(fr)][ch.attrib['dir']] = []
		links[ji][str(fr)][ch.attrib['dir']].append(ch.attrib['linkIndex'])
		# sys.exit(0)

pprint.pprint(state)
pprint.pprint(links)
# sys.exit(0)


# vals = {'l' = 1}

for l in links:
	# print l
	query = "UPDATE `utmc_signal_movements` SET `linkIndex`='' WHERE `SignalSCN` = '%s'" %(l)
	cur.execute(query)
	vals = links[l]
	# pprint.pprint(vals)
	for v in vals:
		# print v
		# print vals[v]
		query = "SELECT * FROM `utmc_signal_movements` WHERE `SignalSCN` = '%s' AND `from_link` = %s" %(l,v)
		cur.execute(query)
		res = cur.fetchall()
		# print res
		# print len(res)
		for d in vals[v]:
			# print con.split('_')
			# ind = con.split('_')
			if d == 'l':
				to_link = int(v) - 1
				if to_link == 0:
					to_link = len(res) + 1 + to_link
			if d == 'r':
				to_link = int(v) + 1
				if to_link > (len(res) + 1):
					to_link = 1
			if d == 'L':
				to_link = int(v) - 2
				if to_link <= 0:
					to_link = len(res) + 1 + to_link
			if d == 'R':
				to_link = int(v) + 2
				if to_link > (len(res) + 1):
					to_link = to_link - (len(res) + 1)
			if d == 's':
				# print vals.keys()
				if 'L' in vals[v].keys():
					to_link = int(v) - 3
					if to_link <= 0:
						to_link = len(res) + 1 + to_link
				else:
					to_link = int(v) - 2
					if to_link <= 0:
						to_link = len(res) + 1 + to_link

			query = "SELECT `linkIndex` FROM `utmc_signal_movements` WHERE `SignalSCN` = '%s' AND `from_link` = %s AND `to_link` = %s" %(l,v,str(to_link))
			# print "=========", d
			# print query
			cur.execute(query)
			oldind = cur.fetchone()
			if oldind == None:
				continue
			oldind = oldind[0]
			# oldind = '18;16;'
			# print oldind
			if oldind == None:
				oldind = ';'.join(vals[v][d]) + ';'
			else:
				# newind = oldind + (';'.join(vals[v][d]) + ';')
				oldlinks = oldind.split(';')[:-1]
				for eachind in vals[v][d]:
					if eachind not in oldlinks:
						oldind = oldind + eachind + ';'
				# print oldlinks
			# print oldind
			# sys.exit(0)

			query = "UPDATE `utmc_signal_movements` SET `linkIndex`='%s' WHERE `SignalSCN` = '%s' AND `from_link` = %s AND `to_link` = %s" %(oldind, l,v,str(to_link))
			print query
			cur.execute(query)


# sys.exit(0)

# for

con.close()
