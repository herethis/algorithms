import xml.etree.ElementTree as etree
import sys
import numpy as np

netfile = "GRP003/network0.xml"
# netfile = "JaipurHandDrawn.net.xml"
tree = etree.parse(netfile)
root = tree.getroot()
tags = []
ids = {}
heads = {}
connects = {}
# ids['start'] = 0
ext_edges = []

for ch in root:
	if ch.tag == 'edge':
		if 'from' in (ch.attrib).keys():
			print ch.attrib['id']
			ext_edges.append(ch.attrib['id'])
connects = {}
fromedges = []
toedges = []
for ch in root:
	if ch.tag == 'connection':
		fr = ch.attrib['from']
		to = ch.attrib['to']
		if fr in ext_edges and to in ext_edges:
			if fr not in connects.keys():
				connects[fr] = []
			if to not in connects[fr]:
				connects[fr].append(to)
				if fr not in fromedges:
					fromedges.append(fr)
				if to not in toedges:
					toedges.append(to)

name = "GRP003/Online/turn_ratio_new.xml"
turnt = etree.Element("turns")
tree = etree.ElementTree(turnt)
inter =etree.SubElement(turnt, "interval")
inter.attrib["begin"] = "0"
inter.attrib["end"] = "7200"
fromedges = []
toedges = []
print "Starting"

for xid in connects:
	# print xid print connects[xid]
	fr =etree.SubElement(inter, "fromEdge")
	fr.attrib["id"] = xid
	pr = 1.0/len(connects[xid])
	if xid not in fromedges:
		fromedges.append(xid)
	# to =etree.SubElement(temp, "toEdge") to = {}
	for sub in connects[xid]:
		# print fr.tag print fr.attrib for node in fr:
		# 	# print fr.tag print fr.attrib
		# 	if fr.attrib["id"] == xid:
		to =etree.SubElement(fr, "toEdge")
		to.attrib["id"] = sub
		if sub not in toedges:
			toedges.append(sub)
		to.attrib["probability"] = str(pr)

sink =etree.SubElement(turnt, "sink")
# sinks = ["-J1A1","-J1A2", "-J2A2", "gneE187", "gneE188","gneE189","gneE190","gneE191", "-28701464#31.7", "28786078#5.20"]
sinks = list(set(toedges) - set(fromedges))
print list(set(fromedges) - set(toedges))
sink.attrib["edges"] = ' '.join(sinks)
tree.write(open(name, 'wb'))
