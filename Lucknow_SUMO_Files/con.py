import MySQLdb
import time

class Con:
    global conn
    global conn_tis
    def __init__(self):
        #print "Setting up connection"
        self.conn = MySQLdb.connect("10.105.104.104","root","itspe","htms")
        self.conn_tis = MySQLdb.connect("10.105.104.104","root","itspe","tis")
        self.cur = self.conn.cursor()
        self.cur_tis = self.conn_tis.cursor()
        self.conn.autocommit(True)
        self.conn_tis.autocommit(True)
    def close(self):
        #print "Close connection"
        self.conn.close()
        self.conn_tis.close()
