import datetime
import os
import xml.etree.ElementTree as etree
import math
import sys
from con import Con
import calendar
import numpy as np
import time
import subprocess
from pprint import pprint
import copy

MAIN_FOLDER_PATH = '/home/atcsadmin' #os.getcwd()
SUMO_TOOLS_PATH = MAIN_FOLDER_PATH + "/sumo-1.0.1/sumo/tools"
SUMO_BINARY_PATH = MAIN_FOLDER_PATH + "/sumo-1.0.1/bin/sumo"

code_folder = '/home/atcsadmin/Lucknow_SUMO_Files/'

sys.path.append(SUMO_TOOLS_PATH)  # Computer Specific.
pathy = MAIN_FOLDER_PATH  # Computer Specific
#sumobinarypath = SUMO_BINARY_PATH
#sys.path.insert(0, pathy)

#from sumolib import checkBinary
#import traci

vehRef = code_folder + "VehRef.xml"

# turnRatXml = "turn_ratio_new.xml"
# netFile =  "network0.xml"

# jtrflowfile = "FlowFile.xml"
# jtrroufile = "Routes.Rou.xml"
# updatedlogic = "UpdatedtlLogic.xml"

# outfile = "output0.xml"

vehTypRat ={"car":18.03,"bus":1.00,"2w":73.18,"auto":1.13,"tempo":0.86,"tractor":2.12,"truck":3.44,"other":0.24}

# simtim = 45
# profile = False
# run_every = 10

# timeconsidered = simtim - low - high 
# addtim = (simtim/6.0) + (5 - (simtim/6.0)%5)
# low = addtim*60
# high = (simtim - addtim)*60

# fromEdgeNum = {"J1A1": 100, "J1A2": 100,"J2A2": 100, "J3A3": 100,"-28701475#3": 100, "-28701468#1":100,"-28786078#13":100, "28701464#31":100, "28786078#0":100}

# fromEdgeNum = {"J1A1": 100, "J1A2": 100, "J2A2": 100, "-gneE187": 100, "-gneE188": 100, "-gneE189": 100, "-gneE190": 100, "-gneE191": 100, "28701464#31": 100, "-28786078#13": 100}
# fromEdgeNum = {'J015_L03': 100, 'J017_L03': 100, 'J017_L02': 100, 'J016_L01': 100, 'J016_L04': 100, 'J015_L04': 100, 'J014_L01': 100, 'J014_L02': 100, 'J016_L03': 100}

vehTypRat ={"car":18.03,"bus":1.00,"2w":73.18,"auto":1.13,"tempo":0.86,"tractor":2.12,"truck":3.44,"other":0.24}

def get_veh_data_list(vehref):
	"""

	:param vehref:veh ref file
	:return: list with veh attributes data
	"""
	print("Inside get_veh_data_list")
	tree = etree.parse(vehref)
	root = tree.getroot()
	alist =[]
	for child in root:
		if child.tag == "vType":
			alist.append(child.attrib)

	# for i in alist:
	# 	print(i)
	#sys.exit(0)
	return alist

def create_flow_file(fromTime, toTime, fromEdgeNum, vehTypRat, vehRef, name):

	flowList =[]
	for fromEdge in fromEdgeNum:
		totNum =fromEdgeNum[fromEdge]
		for vehTyp in vehTypRat:
			edge =str(fromEdge)
			num =str(int(math.ceil(totNum*vehTypRat[vehTyp]/100.0)))
			typ =vehTyp
			ids =str(fromTime)+"_"+str(toTime)+"_"+str(fromEdge)+"_"+str(vehTyp)
			dictFlowElement ={"id":ids,"from":edge,"number":num,"type":typ}
			flowList.append(dictFlowElement)

	#print(flowList)

	flows =etree.Element("flows")
	tree = etree.ElementTree(flows)
	#print(flows)

	vehTypLst = get_veh_data_list(vehRef)
	#print(vehTypLst)
	for vehTypElement in vehTypLst:
		# print (vehTypElement)
		vehType =etree.SubElement(flows, "vType")
		for key in vehTypElement:
			# print (key)
			vehType.attrib[key] = vehTypElement[key]
	#sys.exit(0)

	interval =etree.SubElement(flows,"interval")
	interval.attrib["begin"] =fromTime
	interval.attrib["end"] =toTime
	for flowElement in flowList:
		flowEle =etree.SubElement(interval,"flow")
		flowEle.attrib["id"] =flowElement["id"]
		flowEle.attrib["from"] =flowElement["from"]
		flowEle.attrib["number"] =flowElement["number"]
		flowEle.attrib["type"] =flowElement["type"]
	tree.write(open(name, 'wb'))
	# file =open(name,"w")
	# print(prettify(flows),file=file)
	# #print(flows, file ="flochecker.xml")
	# file.close()

def getEdgetoDetect(fromedges):
	edge_to_detect = {}
	for edge in fromedges:
		edge_to_detect[edge] = [edge+'_03', edge+'_02',edge+'_01']
	return edge_to_detect

def getProfile(link, timenew, dur, type_desc):
	con = Con()
	cur = con.cur
	# dow = timenew.weekday()
	# type_desc = calendar.day_name[dow]
	query = "SELECT `TypeID` FROM `utmc_day_typeid` WHERE `TypeDescription` = '%s'" %(type_desc)
	cur.execute(query)
	typeid = cur.fetchone()[0]
	query = "SELECT `ProfileId` FROM `utmc_detector_profile` WHERE `DayTypeId` = '%d' AND `ProfileId` LIKE '%%%s%%'" %(typeid,link)
	# print query
	cur.execute(query)
	profid = cur.fetchone()
	if profid == None:
		return None
	# profid = [0]
	# timenew= timenew.time()
	query = "SELECT `TotalFlow` FROM `utmc_detector_profile_data` WHERE `ProfileID` = '%s' AND `SystemCodeNumber` = '%s' AND `StartTime` >= '%s' AND `StartTime` < '%s'" %(profid[0], link, str(timenew.time()),str((timenew + datetime.timedelta(minutes = dur)).time()))
	cur.execute(query)
	th = cur.fetchall()
	if len(th) == 0:
		th = None
	else:
		th = np.sum(th)
	con.close()
	return th

def getDynamicDet(link, timenew, dur):
	con = Con()
	cur = con.cur
	query = "SELECT `TotalFlow` FROM `utmc_detector_dynamic` WHERE `SystemCodeNumber` = '%s' AND `LastUpdated` > '%s' AND `LastUpdated` <= '%s'" %(link, str(timenew),str(timenew + datetime.timedelta(minutes = dur)))
	cur.execute(query)
	print(query)
	results = cur.fetchall()
	# print(results)
	# sys.exit(0)
	if len(results) == 0:
		fin = None
	else:
		fin = np.sum(results)
	con.close()
	return fin


def getutcReply(dets, timenow, simtim):
	con = Con()
	cur = con.cur
	cur_tis = con.cur_tis
	# print dets
	timenow = timenow - datetime.timedelta(minutes = ((5*60) + 30))
	dets = list(np.sort(dets))
	detscn = ';'.join(dets) + ';'
	# detscn = detscn + ;
	# print detscn
	#detscn = 'J018_L04_01;J018_L04_02;'
	#timenow = datetime.datetime.strptime('2019-02-02 12:27:33', '%Y-%m-%d %H:%M:%S')
	query = "SELECT `SignalSCN`,`ug405DetectorBitNumber` FROM `utmc_signal_movements` WHERE `DET_SCN` = '%s'" %(detscn)
	cur.execute(query)
	det_data = cur.fetchone()
	if det_data == None:
		return None
	query = "SELECT `site_id` FROM `utmc_traffic_signal_static` WHERE `SignalSCN` = '%s'" %(det_data[0])
	cur.execute(query)
	siteid = cur.fetchone()
	if siteid == None:
		return None
	siteid = siteid[0]
	query = "SELECT `reply_timestamp`,`utcReplySDn` FROM `utcReplyTable` WHERE `site_id` = '%d' AND `reply_timestamp` > '%s' AND `reply_timestamp` <= '%s' AND `utcReplySDn` is NOT NULL ORDER BY `reply_timestamp` ASC" %(siteid, str(timenow), str(timenow + datetime.timedelta(minutes = simtim)))
	# print query
	cur_tis.execute(query)
	results = cur_tis.fetchall()
	# print results
	# dets = det_data[0].split(";")
	bits = det_data[1].split(";")[:-1]
	if bits[0] == '':
		return None
	time_taken = 0.0
	counts = 0.0
	if len(results) == 0:
		return None
	if len(results) > 1:
		for res in range(1,len(results)):
			time_taken = time_taken + (results[res][0] - results[res-1][0]).seconds
			for bit in bits:
				temp_count = (int(results[res][1][int(bit)],16) - int(results[res-1][1][int(bit)],16))
				if temp_count < 0:
					temp_count = 15 + temp_count
				counts = counts + temp_count
		# flow_now = counts*60.0/time_taken
	# print counts
	con.close()
	# print counts
	return counts

def checkdetectorexists(edge):
	con = Con()
	cur = con.cur
	junc = edge[:4]
	timetocheck = datetime.datetime.now() - datetime.timedelta(days = 1)
	query = "SELECT SUM(`TotalFlow`) FROM `utmc_detector_dynamic` WHERE `LastUpdated` > '%s' AND `SystemCodeNumber` LIKE '%%%s%%'" %(str(timetocheck), str(junc))
	print query
	cur.execute(query)
	total_val = cur.fetchall()
	print total_val
	total_val = total_val[0][0]
	if total_val == 0 or total_val == None:
		query = "SELECT `TotalFlow` FROM `utmc_detector_dynamic` WHERE `TotalFlow` <> 0 ORDER BY `LastUpdated` DESC, `TotalFlow` DESC LIMIT 20"
		cur.execute(query)
		flow_vals = cur.fetchall()
		temp_val_min = flow_vals[-1][0]*9
		temp_val_max = flow_vals[9][0]*9
		temp_val = np.random.randint(temp_val_min, temp_val_max)
	else:
		temp_val = 0

	con.close()
	return temp_val

def getfromEdgeNum(fromEdgeNum, startTm, profile, simtim, daydesc):
	# fromEdgeNum = {'J015_L03': 100, 'J017_L03': 100, 'J017_L02': 100, 'J016_L01': 100, 'J016_L04': 100, 'J015_L04': 100, 'J014_L01': 100, 'J014_L02': 100, 'J016_L03': 100}
	edge_to_detect = getEdgetoDetect(fromEdgeNum.keys())
	# print edge_to_detect
	# print startTm.time()
	for edge in fromEdgeNum:
		detects = edge_to_detect[edge]
		val = []
		if profile:
			for det in detects:
				temp_val = getProfile(det,startTm, simtim, daydesc)
				print(temp_val)
				if temp_val == None:
					# temp_val = [np.random.randint(40,60)]
					temp_val = 0
				val.append(temp_val)
		else:
			for det in detects:
				temp_val = getDynamicDet(det,startTm, simtim)
				# print(temp_val, det)
				if temp_val == None:
					# temp_val = [np.random.randint(40,60)]
					temp_val = 0
				val.append(temp_val)
			# temp_val = getutcReply(detects, startTm)
		if np.sum(val) == 0:
			fromEdgeNum[edge] = checkdetectorexists(edge)
		else:
			fromEdgeNum[edge] = np.sum(val)
	# print(fromEdgeNum)
	# sys.exit(0)
	return fromEdgeNum

def create_new_routefile_main(startTm, endTm, fromEdgeNum, jtrflowfile, turnRatXml ,netFile, jtrroufile):
	"""

	:param startTm:
	:param endTm:
	:param fromEdgeNum:
	:return:
	"""

	fromTime = "0"
	toTime = str((endTm - startTm).total_seconds())
	create_flow_file(fromTime, toTime, fromEdgeNum, vehTypRat,vehRef,jtrflowfile)
	#os.system ("jtrrouter --flow-files=FlowFile.xml --turn-ratio-files=turnRatios.xml --net-file=JaipurHandDrawn.net.xml --output-file=Routes.Rou.xml")
	#os.system ("jtrrouter --flow-files=FlowFile.xml --turn-ratio-files="+str(turnRatXml)+" --net-file="+str(netFile)+" --output-file=Routes.Rou.xml")
	os.system ("jtrrouter --flow-files=%s --turn-ratio-files=%s --net-file=%s --output-file=%s"%(jtrflowfile,
			turnRatXml,netFile,jtrroufile))
	jtr_ele = etree.parse(jtrroufile)
	jtr_root = jtr_ele.getroot()
	print jtr_root
	print jtr_root.attrib
	#del jtr_root.attrib[:]
	att_dict = copy.deepcopy(jtr_root.attrib)
	for att in att_dict:
		jtr_root.attrib.pop(att, None)
	jtr_ele.write(open(jtrroufile, 'wb'))
	#sys.exit()
	# logfile = open(psoLog, "a+")
	# logfile.write("From Edge Number Dict:\n%s\n" % str(fromEdgeNum))
	# logfile.write("from time: %s\n" % (fromTime))
	# logfile.write("to time: %s\n\n" % (toTime))
	# logfile.write("flow file created: %s\nroute file created: %s\n\n" % (jtrflowfile,jtrroufile))
	# logfile.close()

def getMoreFlowInfo(timenow, simtim, turnRatXml, jtrroufile, outfile, fromEdgeNum, daydesc, profile, prev_edge):

	addtim = (simtim/6.0) + (5 - (simtim/6.0)%5)
	low = addtim*60
	high = (simtim - addtim)*60

	print(outfile)
	time.sleep(2)
	sumoout = etree.parse(outfile)
	outroot = sumoout.getroot()
	# print outroot.tag
	# print outroot.attrib
	tripids = []
	for ch in outroot.findall('tripinfo'):
		# print ch.attrib
		if float(ch.attrib['depart']) > low:
			tripids.append(ch.attrib['id'])
		if float(ch.attrib['arrival']) > high:
			break
	# print len(tripids)

	print(addtim, simtim, high, low)
	# print tripids[0]
	# print tripids[-1]
	routetree = etree.parse(jtrroufile)
	root = routetree.getroot()
	edges_count_sim = {}
	for ch in root.findall('vehicle'):
		if ch.attrib['id'] in tripids:
			via = (ch.find('route')).attrib['edges']
			# print via
			via = via.split()
			for ed in via:
				if ed not in edges_count_sim.keys():
					edges_count_sim[ed] = 0
				edges_count_sim[ed] = edges_count_sim[ed] + 1
	edge_to_detect = getEdgetoDetect(edges_count_sim.keys())
	
	print("====simulation count====")
	print(edges_count_sim)
	edges_count_act = {}
	max_diff = 0
	max_edge = 'X'
	for edge in edges_count_sim:
		flagnodet = False
		# if (not profile) and (edge in fromEdgeNum.keys()):
		# 	continue
		# if edge in fromEdgeNum.keys():
		# 	continue
		detects = edge_to_detect[edge]
		val = []
		for det in detects:
			if not profile:
				temp_val = getDynamicDet(det,timenow + datetime.timedelta(minutes = addtim),(simtim-2*addtim))
			else:
				temp_val = getProfile(det,timenow + datetime.timedelta(minutes = addtim),(simtim-2*addtim), daydesc)
			# print(temp_val, det)
			if temp_val == None:
				# temp_val = [np.random.randint(40,60)]
				flagnodet = True
			val.append(temp_val)
		if flagnodet:
			val = None
		else:
			val = np.sum(val)
			if val == 0:
				val = None
			else:
				if edge in edges_count_sim.keys():
					val = (val - edges_count_sim[edge])
					# print("             " + str(val) + ", " + edge)
					if abs(val) > abs(max_diff):
						if edge not in fromEdgeNum.keys():
							max_diff = val
							max_edge = edge
		edges_count_act[edge] = val

	print("====Diff====")
	print(edges_count_act)
	sec_edge = 'X'
	sec_diff = 0
	if max_edge == prev_edge:
		for edge in edges_count_act:
			if edges_count_act[edge] == None:
				pass
			else:
				if edge not in fromEdgeNum.keys():
					if edge == max_edge:
						pass
					else:
						if abs(edges_count_act[edge]) > abs(sec_diff):
							max_diff = edges_count_act[edge]
							max_edge = edge
	# diff = edges_count_act - edges_count_sim
	# print diff
	print(max_diff, max_edge)
	# sys.exit(0)
	if abs(max_diff) < 50:
		return True, max_edge
	turnt = etree.parse(turnRatXml)
	ro_turn = turnt.getroot()
	# print ro_turn
	inter = ro_turn.find('interval')
	# froms = inter.findall('fromEdge')
	# inter = ro_turn.find('fromEdge')
	froms = []
	for ch in inter:
		for gch in ch:
			if gch.attrib['id'] == max_edge:
				froms.append(ch)
	# print froms
	# print "=====Turn Ratios====="
	para_tune = {}
	for ch in froms:
		# print ch.attrib
		# para_tune[ch.attrib['id']] = {}
		temp_dict = {}
		for gch in ch:
			# print ("        ", gch.attrib)
			# print ("        ", edges_count_act[gch.attrib['id']])
			if gch.attrib['id'] not in edges_count_act.keys():
				gchattribact = None
			else:
				gchattribact = edges_count_act[gch.attrib['id']]
			temp_dict[gch.attrib['id']] = [gchattribact,gch.attrib['probability']]
		para_tune[ch.attrib['id']] = temp_dict
	# print "===Tunes====="
	# print para_tune
	if max_diff < 0:
		mul_fact = -1
	else:
		mul_fact = 1
	# print temp_dict
	# print(len(para_tune))
	denom = edges_count_sim[max_edge]
	if edges_count_sim[max_edge] < (edges_count_sim[max_edge] + max_diff):
		denom = (edges_count_sim[max_edge] + max_diff)
	factor = 1.0*abs(max_diff)/(denom*len(para_tune))
	print(factor)
	# print para_tune.keys()

	for ch in inter:
		# print ch.attrib['id']
		if ch.attrib['id'] in para_tune.keys():
			tos = ch.findall('toEdge')
			tot_tos = len(tos)
			tot_prob = 0
			count_tos = 1
			for gch in ch:
				if count_tos == tot_tos:
					new_prob = 1 - tot_prob
				elif gch.attrib['id'] == max_edge:
					new_prob = (float(gch.attrib['probability'])) + (mul_fact*factor)
					tot_prob = tot_prob + new_prob
				else:
					new_prob = (float(gch.attrib['probability'])) + (mul_fact*factor*-1)/(tot_tos - 1)
					tot_prob = tot_prob + new_prob
				if new_prob < 0.01:
					tot_prob = tot_prob - new_prob
					new_prob = 0.01
					tot_prob = tot_prob + new_prob
				if tot_prob > 1:
					extra = tot_prob - 1 + ((tot_tos - count_tos)*0.01)
					new_prob = new_prob - extra
					tot_prob = 1 - ((tot_tos - count_tos)*0.01)
				gch.attrib['probability'] = str(new_prob) 
				count_tos = count_tos + 1
				# froms.append(ch)
				# print gch.attrib
	# sys.exit(0)
	turnt.write(open(turnRatXml, 'wb'))
	return False, max_edge

# import traci

def run_one_simulation(config_num=8813, STEPS=900, base_file = "/"):
    print("Inside run one")
    os.system("sumo " + base_file + "/0.sumo.cfg")

def updatetlLogic(netFile, updatedlogic, profile, group = '0', daytype = '0', slot = 0):
	con = Con()
	cur = con.cur
	netin = etree.parse(netFile)
	netroot = netin.getroot()
	# tllogic = {}

	add_tl =etree.Element("additional")
	tree = etree.ElementTree(add_tl)

	for ch in netroot.findall('tlLogic'):
		# print ch.tag
		# print ch.attrib
		sig = ch.attrib['id']
		# x_sig = sig
 		# sig = "STJOHN"
		query = "SELECT `SignalOffset` FROM `utmc_traffic_signal_static` WHERE `SignalSCN` = '%s'" %(sig)
		# query = "SELECT `SignalOffset` FROM `utmc_traffic_signal_static` WHERE `SignalSCN` = '%s'" %('ABC')
		cur.execute(query)
		result = cur.fetchone()
		if result == None:
			new_off = ch.attrib['offset']
		else:
			new_off = str(result[0])

		if not profile:
			query = "SELECT group_concat(`signal_timings`.`runStageTime` ORDER BY execOrder separator ',') as StageTime,`utmc_traffic_signal_static`.`is_active`,group_concat(`signal_timings`.`runInterStageTime` ORDER BY execOrder separator ',') as InterStageTime,`utmc_traffic_signal_static`.`ForceBidNextStagePin`,`utmc_traffic_signal_static`.`currentMode`,`plans`.`PlanSCN`,`plans`.`CycleTime`,utmc_traffic_signal_static.`currentPlan`,utmc_traffic_signal_static.`defaultPlan`,group_concat(`signal_timings`.`StageNumber` ORDER BY execOrder separator ',') as StageNumber,utmc_traffic_signal_static.`oldPlanId`, plans.`CriticalStage` FROM `utmc_traffic_signal_static` INNER JOIN `plans` ON `plans`.`ID`=`utmc_traffic_signal_static`.`currentPlan` INNER JOIN `signal_timings` ON `signal_timings`.`SignalSCN`='%s' AND `signal_timings`.`Plan_SCN`=`plans`.`PlanSCN` AND execOrder <> 0 INNER JOIN `utmc_traffic_signal_stages` ON `utmc_traffic_signal_stages`.`SignalSCN` = `utmc_traffic_signal_static`.`SignalSCN` AND `utmc_traffic_signal_stages`.`SignalSCN`='%s' AND `utmc_traffic_signal_stages`.`StageOrder`=`signal_timings`.`StageNumber` WHERE `utmc_traffic_signal_static`.`SignalSCN`='%s'" %(sig, sig, sig)
			print(query)
			cur.execute(query)
			signal_dynamic = cur.fetchone()
			print(signal_dynamic)
			if signal_dynamic[1] == None:
				continue
			stages = signal_dynamic[9].split(',')
			stagetimes = signal_dynamic[0].split(',')
			intertimes = signal_dynamic[2].split(',')

		else:
			query = "SELECT signal_timings.SignalSCN,TimeTable_SCN, Slot_Order,calendar.Plan_SCN,StartTime,EndTime,signal_timings.StageTime,signal_timings.InterStageTime,signal_timings.StageOrder FROM calendar INNER JOIN time_tables ON time_tables.TimeTableSCN = calendar.TimeTable_SCN AND time_tables.SlotOrder = calendar.Slot_Order INNER JOIN (select group_concat(a.SignalSCN separator ';') as SignalSCN, group_concat(a.StageTime separator ';') as StageTime, group_concat(a.InterStageTime separator ';') as InterStageTime, group_concat(a.runStageTime separator ';') as runStageTime, group_concat(a.runInterStageTime separator ';') as runInterStageTime,group_concat(a.StageOrder separator ';') as StageOrder,a.Plan_SCN from (select SignalSCN, Plan_SCN, group_concat(StageTime ORDER BY execOrder separator ',') as StageTime, group_concat(InterStageTime ORDER BY execOrder separator ',') as InterStageTime, group_concat(runStageTime ORDER BY execOrder separator ',') as runStageTime, group_concat(runInterStageTime ORDER BY execOrder separator ',') as runInterStageTime, group_concat(StageNumber ORDER BY execOrder separator ',') as StageOrder from signal_timings where execOrder <> 0 group by SignalSCN,Plan_SCN ) a GROUP BY a.Plan_SCN) signal_timings ON signal_timings.Plan_SCN = calendar.Plan_SCN WHERE `calendar`.`Date` = '%s' AND `calendar`.`Group_SCN` = '%s' AND `calendar`.`Slot_Order` = %s" %(daytype, group, str(slot))
			print(query)
			cur.execute(query)
			signal_dynamic = cur.fetchone()
			print(signal_dynamic)
			if signal_dynamic[1] == None:
				continue
			stages = signal_dynamic[8].split(',')
			stagetimes = signal_dynamic[6].split(',')
			intertimes = signal_dynamic[7].split(',')

		# query = "SELECT `PlanNumber`, `StageSequence`,`PlanTimings`, `LastUpdated` FROM `utmc_traffic_signal_dynamic` WHERE `SystemCodeNumber` = '%s' ORDER BY `LastUpdated` DESC LIMIT 1" %(sig)
		# # print query
		# cur.execute(query)
		# signal_dynamic = cur.fetchone()
		# # print signal_dynamic
		# query = "SELECT `PlanSCN` FROM `plans` WHERE `ID` = %s" %(str(signal_dynamic[0]))
		# cur.execute(query)
		# plan_name = cur.fetchone()
		# print result

		# Find states correspoding to each stage

		count = 0
		states = []
		interstates = []
		for grand_ch in ch:
			# print grand_ch.tag
			# print grand_ch.attrib
			if count%2 == 0:
				states.append(grand_ch.attrib["state"])
			else:
				interstates.append(grand_ch.attrib["state"])
			count = count + 1
		# states = ['rrrrgGGGGrrrrrrr', 'GrrrrrrrrrrrgGGG', 'gGGGGrrrrrrrrrrr', 'rrrrrrrrgGGGGrrr']
		# interstates = ['rrrryyyygrrrrrrr', 'grrrrrrrrrrryyyy', 'yyyygrrrrrrrrrrr', 'rrrrrrrryyyygrrr']
		# print states, interstates

		# stages = signal_dynamic[1].split(',')
		# stagetimes = signal_dynamic[2].split(',')
		# print stages, stagetimes

		logic_elm = etree.SubElement(add_tl,"tlLogic")
		logic_elm.attrib["id"] = sig
		logic_elm.attrib["offset"] = str(new_off)
		logic_elm.attrib["programID"] = "updated_" + sig
		logic_elm.attrib["type"] = "static"

		for stg_count in range(len(stages)):
			new_state = 'r'*len(states[0])
			new_state = list(new_state)
			new_interstate = 'r'*len(states[0])
			new_interstate = list(new_interstate)
			query = "SELECT `VehicleMovements` FROM `utmc_traffic_signal_stages` WHERE `SignalSCN` = '%s' AND `StageOrder` = %s" %(sig, str(stages[stg_count]))
			#print(query)
			cur.execute(query)
			move = cur.fetchone()
			if move == None:
				pass
			else:
				move = move[0]
				moves = move.split(';')[:-1]
				for veh_mov in moves:
					query = "SELECT `linkIndex` FROM `utmc_signal_movements` WHERE `id` = %s" %(veh_mov)
					#print(query)
					cur.execute(query)
					g_cons = cur.fetchone()
					g_cons = g_cons[0]
					#print(g_cons)
					if g_cons == None:
						pass
					else:
						g_cons = g_cons.split(';')[:-1]
						for gs in g_cons:
							gs = int(gs)
							# print new_state, gs, new_state[gs]
							# print type(new_state), type(gs), type(new_state[gs])
							new_state[gs] = 'G'
							new_interstate[gs] = 'y'
			new_state = "".join(new_state)
			new_interstate = "".join(new_interstate)
			if int(stagetimes[stg_count]) == 0:
				pass
			else:
				phase_elem = etree.SubElement(logic_elm, "phase")
				phase_elem.attrib["duration"] = str(stagetimes[stg_count])
				phase_elem.attrib["state"] = new_state
				# if stg_count >= (len(interstates)):
				# 	continue
				if int(intertimes[stg_count]) == 0:
					pass
				else:
					phase_elem = etree.SubElement(logic_elm, "phase")
					# print stg_count, len(interstates), sig

					phase_elem.attrib["duration"] = str(intertimes[stg_count])
					phase_elem.attrib["state"] = new_interstate
			# sys.exit(0)
	
	pprint(tree)
	tree.write(open(updatedlogic, 'wb'))
	con.close()
	# netin.write(netFile)
	# print tllogic

def simulation(fromEdgeNum, startTm, endTm, simtim, profile, daydesc, jtrflowfile, turnRatXml ,netFile, jtrroufile, base_file, updatedlogic, outfile, group, slot = 1):
	# if not profile:
	# sys.exit(0)
	# startTm = startTm - datetime.timedelta(minutes = addtim)
	# endTm = endTm + datetime.timedelta(minutes = addtim)
	# print (endTm, startTm)
	# getMoreFlowInfo(startTm)
	# run_one_simulation(0, simtim*60)
	# sys.exit(0)
	fromEdgeNum = getfromEdgeNum(fromEdgeNum, startTm, profile, simtim, daydesc)
	print fromEdgeNum
	no_veh = True
	for key in fromEdgeNum:
		if fromEdgeNum[key] > 0:
			no_veh = False
			break
	if no_veh == True:
		return
	# if not profile:
	updatetlLogic(netFile, updatedlogic, profile, group, daydesc, slot)
	counter = 0
	out_flag = False
	max_edge = 'X'
	while True:
		print("==========counter===", counter, "===============")
		create_new_routefile_main(startTm, endTm, fromEdgeNum, jtrflowfile, turnRatXml ,netFile, jtrroufile)
		print(fromEdgeNum)
		# sys.exit()
		addtim = (simtim/6.0) + (5 - (simtim/6.0)%5)
		low = addtim*60
		high = (simtim - addtim)*60
		high = 1000
		run_one_simulation(0, high, base_file)
		# print fromEdgeNum
		sub_count = 0
		while True:
			keepg, max_edge = getMoreFlowInfo(startTm, simtim, turnRatXml, jtrroufile, outfile, fromEdgeNum, daydesc, profile, max_edge)
			sub_count = sub_count + 1
			if keepg:
				out_flag = True
			if keepg or (sub_count > 10):
				break
		counter = counter + 1
		if keepg or (counter > 5) or out_flag:
			break

def get_edges(file):
	tree = etree.parse(file)
	root = tree.getroot()
	ext_edges = []

	for ch in root:
		if ch.tag == 'edge':
			if 'from' in (ch.attrib).keys():
				# print ch.attrib['id']
				ext_edges.append(ch.attrib['id'])

	connects = {}

	fromedges = []
	toedges = []

	for ch in root:
		if ch.tag == 'connection':
			fr = ch.attrib['from']
			to = ch.attrib['to']
			if fr in ext_edges and to in ext_edges:
				if fr not in connects.keys():
					connects[fr] = []
				if to not in connects[fr]:
					connects[fr].append(to)
					if fr not in fromedges:
						fromedges.append(fr)
					if to not in toedges:
						toedges.append(to)

	return fromedges, toedges

def check_network(netFile, newFile):

	# print netFile, newFile

	from1, to1 = get_edges(netFile)
	from2, to2 = get_edges(newFile)

	if ((from1 == from2) & (to1 == to2)):
		return list(set(from1) - set(to1))
	else:
		return []


def run_trfilecode(group, newFile = '0', profile = False, daytype = '0', timeslot = '0', slot = 1):

	# global netFile, turnRatXml, jtrflowfile, jtrroufile, updatedlogic, base_file
	# global profile
	# global simtim, addtim, low, high

	# global MAIN_FOLDER_PATH, SUMO_TOOLS_PATH, SUMO_BINARY_PATH, pathy, sumobinarypath

	# profile = prof

	# print newFile

	netFile = code_folder + group + "/network0.xml"
	if newFile == '0':
		newFile = netFile
	if not profile:

		simtim = 45

		endTm = datetime.datetime.now() + datetime.timedelta(hours = 5.5)
		print(endTm)
		#endTm = datetime.datetime.strptime("2019-02-18 09:27:12", "%Y-%m-%d %H:%M:%S")
		corr = (endTm.minute%5)*60
		endTm = (endTm - datetime.timedelta(seconds = corr)).replace(microsecond = 0, second = 0)
		print(endTm)
		base_file = code_folder + group +"/Online"
		#turnRatXml = group + "/Online/turn_ratio_new.xml"
		# netFile =  "network0.xml"

		jtrflowfile = group + "/Online/FlowFile.xml"
		#jtrroufile = group + "/Online/Routes.Rou.xml"
		#updatedlogic = group + "/Online/UpdatedtlLogic.xml"

		#outfile = group + "/Online/output0.xml"

	if profile:

		# endTm = endTm - datetime.timedelta(endTm.time())

		base_file = code_folder + group + "/" + daytype + "/" + timeslot

		sta, end = timeslot.split("_")
		stas = sta.split('H')
		stas = 60*int(stas[0]) + int(stas[1])
		ends = end.split('H')
		ends = 60*int(ends[0]) + int(ends[1])
		if end == "24H00":
			end = "23H59"
			timeslotx = "_".join([sta, end])
		else:
			timeslotx = "_".join([sta, end])
		endTm = datetime.datetime.strptime(timeslotx.split("_")[1],"%HH%M")
		# print sta, end
		simtim = ends - stas

	turnRatXml = base_file + "/turn_ratio_new.xml"
	jtrflowfile = base_file + "/FlowFile.xml"
	jtrroufile = base_file + "/Routes.Rou.xml"
	updatedlogic = base_file + "/UpdatedtlLogic.xml"
	outfile = base_file + "/output0.xml"
	
	print(endTm)
	startTm = endTm - datetime.timedelta(minutes = simtim)
	print(startTm, endTm)

	# print netFile, newFile

	from_e = check_network(netFile, newFile)

	if len(from_e) == 0:
		print( "Error")
	else:

		fromEdgeNum = {}

		for fr in from_e:
			fromEdgeNum[fr] = 100

		print(fromEdgeNum)
		# sys.exit(0)

		# while True:
		simulation(fromEdgeNum, startTm, endTm, simtim, profile, daytype, jtrflowfile, turnRatXml ,netFile, jtrroufile, base_file, updatedlogic, outfile, group, slot)
			# time.sleep(run_every*60)
